from django.conf.urls import patterns, include, url
from KrishiApp.views import *
# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()
from KrishiApp.views_sparrow import *


urlpatterns = patterns('',
    (r'^sparrow/?$', get_request),
    #(r'^sparrow_vendor/?$', get_vendor),
    #(r'^chart/?$', return_chart),

    # Examples:
    url(r'^/?$', 'KrishiApp.views.show_price'),
    url(r'^item/(?P<item_id>\d+?)/?','KrishiApp.views.item_page'),
    url(r'^seller/(?P<seller_id>\d+?)/?','KrishiApp.views.seller_page'),
    url(r'^test/?$', 'KrishiApp.views.test'),
    # url(r'^KrishiUtthan/', include('KrishiUtthan.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
)
