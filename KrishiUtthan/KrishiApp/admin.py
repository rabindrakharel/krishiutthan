from django.contrib import admin
from models import *

admin.site.register(Seller)
admin.site.register(Item)
admin.site.register(Price)
admin.site.register(Category)
